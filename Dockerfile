FROM php:5-fpm
MAINTAINER snappy.kop@gmail.com (http://snappytux.com)

ENV PHP_CONF /usr/local/etc/php/conf.d
ENV BUILD_PACKAGES libmcrypt-dev libfreetype6-dev libgd-dev libcurl4-gnutls-dev php5-dev

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y $BUILD_PACKAGES --no-install-recommends \
    && docker-php-ext-install \
    mcrypt \
    mbstring \
    curl \
    gd \
    pdo \
    pdo_mysql \
    json \
    tokenizer \
    zip \
    && apt-get install -y php5-cli php5-curl php-pear php5-mongo supervisor --no-install-recommends \
    && pecl install mongo

# Install nginx
RUN apt-get update
RUN apt-get install -y nginx

# Clear packet
RUN apt-get -y autoremove && apt-get remove --purge -y $BUILD_PACKAGES && rm -r /var/lib/apt/lists/*

# Supervisor
COPY conf/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN mkdir -p /var/log/supervisor

# Enable mongoDB
RUN touch $PHP_CONF/mongo.ini && echo 'extension=mongo.so' > $PHP_CONF/mongo.ini

# Copy our nginx config
RUN rm -Rf /etc/nginx/nginx.conf /etc/nginx/sites-available/default
ADD conf/nginx.conf /etc/nginx/nginx.conf
ADD conf/nginx-site.conf /etc/nginx/sites-available/default

# tweak php config
ADD conf/myphp.ini $PHP_CONF
# RUN sed -i -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" $PHP_INI

# Change index file and add phpinfo
RUN mv /var/www/html/index.nginx-debian.html /var/www/html/index.html
RUN touch /var/www/html/info.php && echo '<?php phpinfo(); ?>' > /var/www/html/info.php

EXPOSE 80 443
CMD ["/usr/bin/supervisord"]
